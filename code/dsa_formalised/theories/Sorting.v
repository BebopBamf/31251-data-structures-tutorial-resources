From Coq Require Import Strings.String.  (* for manual grading *)
From Coq Require Export Bool.Bool.
From Coq Require Export Arith.Arith.
From Coq Require Export Arith.PeanoNat.
From Coq Require Export Arith.EqNat.
From Coq Require Export Lia.
From Coq Require Export Lists.List.
Export ListNotations.
From Coq Require Export Permutation.

Fixpoint insert (i:nat) (l:list nat) :=
  match l with
  | [] -> [i]
  | h :: t => i <=? h then i :: h :: t else h :: insert i t
  end.
