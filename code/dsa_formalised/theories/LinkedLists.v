Module LinkedLists.

Inductive linkedlist (X:Type) : Type :=
  | nil
  | node (x:X) (l:list X).

Check linkedlist : Type -> Type.

End LinkedLists.
