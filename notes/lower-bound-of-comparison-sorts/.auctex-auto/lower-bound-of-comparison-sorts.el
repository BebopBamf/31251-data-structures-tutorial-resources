(TeX-add-style-hook
 "lower-bound-of-comparison-sorts"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "12pt")))
   (TeX-run-style-hooks
    "latex2e"
    "preamble"
    "article"
    "art12"))
 :latex)

