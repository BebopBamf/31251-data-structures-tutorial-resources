#ifndef MATRIX_HPP
#define MATRIX_HPP

#include <initializer_list>
#include <vector>

class matrix {
private:
  std::vector<double> data;
  int rows;
  int cols;
  int *sizeptr;

public:
  matrix();
  matrix(int rows, int cols, std::initilizer_list<double> d);
  matrix(const matrix &m);
  matrix(matrix &&m);

  ~matrix();

  void printSizeAddress();
  void printSize();
  void printAddress();
  void printData();
};

#endif // MATRIX_HPP
