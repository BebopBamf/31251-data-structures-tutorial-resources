#include <iostream>
int add(const int &x, const int &y) { return x + y; }

int main() {
  int user_input_1{};
  int user_input_2{};

  std::cout << "Enter a number: ";
  std::cin >> user_input_1;

  std::cout << "Enter another number: ";
  std::cin >> user_input_2;

  std::cout << "The sum of " << user_input_1 << " and " << user_input_2
            << " is " << add(user_input_1, user_input_2) << std::endl;

  std::cout << "The sum of " << user_input_1 << " and " << user_input_2
            << " is " << add(user_input_1, user_input_2) << std::endl;
  return 0;
}
