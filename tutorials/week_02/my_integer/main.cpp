#include <gtest/gtest.h>
#include "myInteger.hpp"

TEST(myInteger, defaultValueZero) {
  myInteger a {};
  EXPECT_EQ(a.getVal(), 0);
}

TEST(myInteger, initializeAndGet) {
  myInteger a {5};
  EXPECT_EQ(a.getVal(), 5);
}

TEST(myInteger, initializeSetAndGet) {
  myInteger a {-1};
  a.setVal(3);
  EXPECT_EQ(a.getVal(), 3);
}

TEST(myInteger, isEvenInputEven) {
  myInteger a {16};
  EXPECT_EQ(a.isEven(), true);
}

TEST(myInteger, isEvenInputOdd) {
  myInteger a {7};
  EXPECT_EQ(a.isEven(), false);
}

TEST(myInteger, lessThanTrueWhenSmaller) {
  myInteger a {3};
  myInteger b {5};
  EXPECT_EQ(a < b, true);
}

TEST(myInteger, lessThanFalseWhenBigger) {
  myInteger a {5};
  myInteger b {3};
  EXPECT_EQ(a < b, false);
}

int main(int argc, char* argv[]) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
