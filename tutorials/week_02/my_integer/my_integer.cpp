#include "my_integer.hpp"

// implement the default constructor
my_integer::my_integer() : val(5) {}

// implement the one-parameter constructor
my_integer::my_integer(int input) : val(input) {}

// modify this to return val
int my_integer::getVal() const { return 0; }

// set val equal to input
void my_integer::setVal(int input) {}

// modify this to return if val is even or not
bool my_integer::is_even() const { return true; }

// modify this to return if the "this" instance val is
// less than other.val
bool my_integer::operator<(const my_integer &other) const {
  return other < val;
}
