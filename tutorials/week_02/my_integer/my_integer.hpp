// We can use whatever name we like in the
// #ifndef statement but:
// 1) Make sure it is unique (not used for
// any other header file) and
// 2) Make sure the same name is used on both lines
#ifndef MY_INTEGER_HPP
#define MY_INTEGER_HPP

#include <vector>

class my_integer {
private:
  // we make sure val is initialsed here
  int val;

public:
  // constructors
  my_integer();
  my_integer(int);

  ~my_integer();

  // getter
  // we indicate (and enforce) that getVal
  // does not change any member variables by
  // putting const after the parameter list
  int getVal() const;

  // setter
  void setVal(int);

  // check if val is even
  bool is_even() const;

  // C++ lets us define our own operators
  // For two my_integers a and b saying
  // a < b will call a.operator<(b)
  // This allows us to use the natural mathematical
  // notation.  Write this function to return
  // true if a.val < b.val and false otherwise.
  bool operator<(const my_integer &) const;
};

#endif // MY_INTEGER_HPP
