#include <iostream>
#include <string>

int main() {
    std::string user_input {};

    std::cout << "Enter some text: ";
    std::cin >> user_input;

    std::cout << "You entered: " << user_input << std::endl;

    return 0;
}
