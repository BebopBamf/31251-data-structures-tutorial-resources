#include <gtest/gtest.h>
#include <initializer_list>

#include "my_vector.hpp"

TEST(Vector, defaultConstructSizeZero) {
    my_vector vec{};
    EXPECT_EQ(vec.size(), 0);
}

TEST(Vector, constructZeroVector) {
    int n = 3;
    my_vector vec(n);
    EXPECT_EQ(vec.size(), n);
    for (int i = 0; i < n; ++i) {
        EXPECT_EQ(vec[i], 0);
    }
}

TEST(Vector, getAndSet) {
    int n = 3;
    my_vector vec(n);
    vec[1] = 2;
    EXPECT_EQ(vec[1], 2);
}

TEST(Vector, arrayDoubling) {
    my_vector vec(2);
    vec.push_back(3);
    EXPECT_EQ(vec[2], 3);
    EXPECT_EQ(vec.size(), 3);
    EXPECT_EQ(vec.capacity(), 4);
}

int main(int argc, char *argv[]) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
