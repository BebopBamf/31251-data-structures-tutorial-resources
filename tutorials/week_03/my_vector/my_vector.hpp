#ifndef MY_VECTOR_HPP
#define MY_VECTOR_HPP

#include <array>
#include <initializer_list>
#include <iostream>

class my_vector {
  private:
    int *m_arr_ptr = nullptr;
    int m_size;
    int m_capacity;

  public:
    // Default Constructor
    my_vector();

    // Construct a vector of size n where all entries are 0
    explicit my_vector(int n);

    // Construct a vector from an initializer list
    my_vector(std::initializer_list<int> vals);

    // Destructor
    ~my_vector();

    void push_back(int);
    void pop_back();
    int back();
    bool empty();

    // Getter for the size
    int size() const;

    // Getter for the capacity
    int capacity() const;

    // Indexing operator.  Return by reference so entry can be modified.
    int &operator[](int);
};
#endif // MY_VECTOR_HPP
