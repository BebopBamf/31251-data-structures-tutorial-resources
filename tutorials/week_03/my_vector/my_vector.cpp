#include "my_vector.hpp"

my_vector::my_vector() : m_size(0), m_capacity(0) {
    m_arr_ptr = new int[m_capacity];
}

my_vector::my_vector(int n) : m_size(n), m_capacity(n) {
    if (n < 0) {
        std::cerr << "Invalid size" << std::endl;
        return;
    }
    // allocate an array for n ints and zero it out
    m_arr_ptr = new int[m_capacity]();
}

// We write this constructor for you to serve as an example
// An initializer_list is a very lightweight container
// which is used to initialize a vector with a list of values
// std::vector<int> vec {1, 2, 3, 4};
// This constructor implements the same functionality for my_vector.
my_vector::my_vector(std::initializer_list<int> vals) {
    m_size = m_capacity = vals.size();
    m_arr_ptr = new int[m_size];
    int i = 0;
    for (int x : vals) {
        m_arr_ptr[i++] = x;
    }
}

my_vector::~my_vector() {
    // free memory
    delete[] m_arr_ptr;
    m_arr_ptr = nullptr;
}

void my_vector::push_back(int val) {
    if (m_size < m_capacity) {
        m_size++;
        m_arr_ptr[m_size] = val;
    } else {
        m_capacity *= 2;
        int *new_arr_ptr = new int[m_capacity];

        std::move(m_arr_ptr, m_arr_ptr + m_size, new_arr_ptr);
        new_arr_ptr[m_size] = val;

        // delete[] m_arr_ptr;
        m_arr_ptr = new_arr_ptr;
        m_size++;
    }
}

void my_vector::pop_back() {}

int my_vector::back() { return 0; }

bool my_vector::empty() { return m_size < 1; }

int my_vector::size() const { return m_size; }

int my_vector::capacity() const { return m_capacity; }

int &my_vector::operator[](int i) {
    return i > 0 && i < m_size ? m_arr_ptr[i] : m_arr_ptr[0];
}
