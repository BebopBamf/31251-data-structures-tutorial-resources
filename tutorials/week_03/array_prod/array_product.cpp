#include "array_product.hpp"

std::vector<int> product_except_self(const std::vector<int> &nums) {
    std::vector<int> result(nums.size(), 1);

    for (size_t i = 0; i < nums.size(); ++i) {
        for (size_t j = 0; j < nums.size(); ++j) {
            if (i != j) {
                result[i] *= nums[j];
            }
        }
    }
    return result;
}

/*
// O(n)
std::vector<int> product_except_self(const std::vector<int> &nums) {
    bool has_zero = false;
    int prod = 1;

    for (const int &num : nums) {
        if (num != 0) {
            prod *= num;
        } else {
            if (has_zero) {
                prod = 0;
                break;
            }
            has_zero = true;
        }
    }

    std::vector<int> result(nums.size());
    for (size_t i = 0; i < nums.size(); ++i) {
        if (nums[i] == 0) {
            result[i] = prod;
        } else {
            if (has_zero) {
                result[i] = 0;
            } else {
                result[i] = prod / nums[i];
            }
        }
    }
    return result;
}
*/
