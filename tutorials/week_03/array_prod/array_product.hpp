#ifndef ARRAY_PRODUCT_HPP
#define ARRAY_PRODUCT_HPP

#include <numeric>
#include <vector>

/**
 * @brief Given an array nums of n integers where n > 1,  return an array
 * output such that output[i] is equal to the product of all the elements of
 * nums except nums[i].
 *
 * @param nums input array
 * @return output array
 */
std::vector<int> product_except_self(const std::vector<int> &);

#endif // ARRAY_PRODUCT_HPP
